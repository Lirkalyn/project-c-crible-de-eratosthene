#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<errno.h>

struct vec {
    unsigned int a [100]; // Création de l'array a.
    unsigned int num; // Permets de contenir le nombre donné par l'utilisateur.
    unsigned int curnum; // Permets de savoir où on en est.
    short int j; // Booléen qui permet de savoir si on a déjà testé une première fois a.
}; // Création de la structure, et des éléments nécessaires pour son bon fonctionnement.

typedef struct vec vec_t;
vec_t remplir (vec_t a, unsigned int mem, unsigned int rep[mem], unsigned int j);
unsigned int remplir2 (unsigned int mem, unsigned int rep[mem]);
vec_t erathosthene (vec_t a, unsigned int mem, unsigned int rep[mem], unsigned int j);
unsigned int calcnumprem (vec_t a);
void afficher_rep(unsigned int mem, unsigned int rep[mem], vec_t a);
unsigned int reponse(unsigned int mem, unsigned int rep[mem], vec_t a, unsigned int j);

void main () {
    vec_t a; // Création d'une instance de a.
    a.j = 0; // Le booléen vaut 0 donc faux.
    unsigned int mem, j; // Création de mem qui contiendra le nombre de cases mémoire a alloué et j qui compte le nombre de nombre premier présent dans rep.
    j = 0; // Il vaut zéro pour l'instant car on n'a pas encore commencé.
    a.curnum = 2; // On commence notre liste de nombre à 2 comme demandée.
    printf("Jusqu'a quel nombre souhaiter vous chercher les nombres entiers ?\n");
    scanf("%u", &a.num); // On stocke le nombre donné dans num.
    printf("\n");
    if (a.num <= 5000){
        mem = 670; // Mise en place d'un nombre définit de casse mémoire.
    }
    else{
        mem = calcnumprem(a); // Calcul du bon nombre de casses mémoire nécessaire.
    }
    unsigned int* rep; 
    rep = calloc(mem, sizeof(unsigned int)); // Création de l'array de réponse avec le bon nombre de casses mémoire et allocation de la mémoire.
    remplir2(mem, rep); // On remplit rep de 0 pour éviter tout problème.
    while (a.curnum < a.num){
        a = remplir (a, mem, rep, j); // On remplit a avec 100 éléments à chaque fois.
        a.j = 1; // Le booléens est vraie maintenant que l'on a fait une première passe de a.
        rep[mem], j = reponse(mem, rep, a, j); // rep stocke les nombres premiers.
    } // Cette boucle permet de remplir petit à petit l'array a pour aller jusqu'au nombre voulu par l'utilisateur.
    afficher_rep(mem, rep, a); // On affiche la liste des nombres premiers.
}

vec_t remplir (vec_t a, unsigned int mem, unsigned int rep[mem], unsigned int j) {
    int i;
    for (i = 0; i < 100; i++){
        a.a[i] = a.curnum;
        a.curnum++;
    } // Cette boucle permet de remplir a avec les 100 éléments nécessaires.
    a = erathosthene(a, mem, rep, j); // On lance Erathosthene.
    return a;
}

unsigned int remplir2 (unsigned int mem, unsigned int rep[mem]) { // Cette fonction remplie rep de 0.
    unsigned int i;
    for (i = 0; i < mem; i++){
        *(rep + i) = 0;
    }
    return rep[mem];
}

vec_t erathosthene (vec_t a, unsigned int mem, unsigned int rep[mem], unsigned int j){
    if (a.j == 0){ // On vérifie le booléen pour savoir si on a déjà testé a une première fois.
        int i, s;
        for (i = 0; i < 100; i++){
            for (s = 0; s < 100; s++){
                if (a.a[i] != 0 && a.a[i] != a.a[s]){ // On vérifie que a est différent de 0 et on évite de tester un nombre avec lui-même.
                    if ((a.a[s] % a.a[i]) == 0){ // On vérifie s'ils sont multiples. S'ils le sont on remplace le multiple par un 0.
                        a.a[s] = 0;
                    }
                }
            }
        }
    }
    else{ // Pareille qu'au-dessus mais ce coup si on teste avec les nombres premiers contenus dans rep uniquement.
        int i, k;
        for (i = 0; i < j; i++){
            for (k = 0; k < 100; k++){
                if (rep[i] != 0){
                   if ((a.a[k] % rep[i]) == 0){
                       a.a[k] = 0;
                    }
                }
            }
        }
    }
    return a;
}

unsigned int calcnumprem (vec_t a){ // Cette fonction permet de calculer le nombre de nombres premiers jusqu'au nombre demandé par l'utilisateur pour avoir le bon nombre de cases mémoire.
    double mem;
    mem = (a.num / log(a.num)) * 1.16;
    return (unsigned int) mem;
}

void afficher_rep(unsigned int mem, unsigned int rep[mem], vec_t a){ // Permets d'afficher la liste de nombre premier.
    unsigned int i;
    for (i = 0; i < mem; i++){
        if (rep[i] != 0 && rep[i] <= a.num){ // On évite d'afficher les 0 et les nombres supérieurs au nombre demander par l'utilisateur.
            printf("%u\n", *(rep + i));
        }
    }
}

unsigned int reponse(unsigned int mem, unsigned int rep[mem], vec_t a, unsigned int j){
    unsigned int i; // Utilisation d'un int possible
    for (i = 0; i < 100; i++){
        if (a.a[i] != 0){ // On vérifie que le nombre n'est pas un 0 et si ce n'est pas le cas on le stocke dans rep.
            rep[j] = a.a[i];
            j++; // j nous permets de constamment savoir où on en est dans les casses mémoire de rep pour éviter les trous et éviter qu'un nombre premier en remplace un autre.
        }
    }
    return rep[mem], j;
}